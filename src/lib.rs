use syn::parse_macro_input;
use quote::quote;
use crate::detail::generate_embedded_decryption_token_stream;
use syn::parse::Parser;

#[proc_macro]
pub fn embed_cipher_bytes(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use proc_macro2::Group;
    use syn::punctuated::Punctuated;
    use syn::LitInt;
    use syn::token::Comma;
    use bytes::BufMut;

    let parsed = parse_macro_input!(input as Group);
    let parsed = parsed.stream();
    type CommaSeparatedU8s = Punctuated<LitInt, Comma>;
    let parsed = CommaSeparatedU8s::parse_terminated.parse2(parsed).unwrap();
    let mut plain_bytes = Vec::<u8>::new();
    for t in parsed {
        let n: u8 = t.base10_parse().unwrap();
        plain_bytes.put_u8(n);
    }
    let ts = generate_embedded_decryption_token_stream(&plain_bytes);
    return (quote! { #ts }).into();
}

#[proc_macro]
pub fn embed_cipher_text(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use syn::LitStr;

    let parsed = parse_macro_input!(input as LitStr);
    let plain_str = parsed.value();
    let plain_bytes = plain_str.as_bytes();
    let ts = generate_embedded_decryption_token_stream(plain_bytes);
    return (quote! { #ts }).into();
}

#[proc_macro]
pub fn embed_cipher_byte_string(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use syn::LitByteStr;

    let parsed = parse_macro_input!(input as LitByteStr);
    let plain_bytes = parsed.value();
    let ts = generate_embedded_decryption_token_stream(&plain_bytes);
    return (quote! { #ts }).into();
}

#[proc_macro]
pub fn embed_cipher_base64_string(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use syn::LitStr;

    let parsed = parse_macro_input!(input as LitStr);
    let plain_str = parsed.value();
    let plain_bytes = base64::decode_config(plain_str, base64::STANDARD).unwrap();
    let ts = generate_embedded_decryption_token_stream(&plain_bytes);
    return (quote! { #ts }).into();
}

#[proc_macro]
pub fn embed_cipher_file(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use syn::LitStr;
    use std::fs::File;
    use std::path::Path;
    use std::io::Read;

    let parsed = parse_macro_input!(input as LitStr);
    let file_name = parsed.value();
    let path = Path::new(&file_name);
    let mut file = File::open(path).unwrap();
    let mut buffer = Vec::<u8>::new();
    file.read_to_end(&mut buffer).unwrap();
    drop(file);
    let ts = generate_embedded_decryption_token_stream(&buffer);
    return (quote! { #ts }).into();
}

mod detail {
    use bytes::{BytesMut, Buf};
    use openssl::symm::{Cipher, Crypter, Mode};
    use quote::TokenStreamExt;
    use openssl::error::ErrorStack;

    pub fn generate_embedded_decryption_token_stream(plain_bytes: &[u8]) -> proc_macro2::TokenStream {
        use quote::quote;

        let (key, iv, cipher_bytes) = encrypt_plain_bytes(Vec::from(plain_bytes));
        let key_token_stream = u8slice_to_token_stream(&key);
        let iv_token_stream = u8slice_to_token_stream(&iv);
        let cipher_bytes_token_stream = u8slice_to_token_stream(&cipher_bytes);

        return (quote! {{
            use openssl::symm::{Crypter, Cipher, Mode};
            use bytes::BytesMut;
            use bytes::buf::Buf;
            use openssl::error::ErrorStack;
            use std::result::Result::{Ok, Err};

            let key = Box::new(&[#key_token_stream][..]);
            let iv = Box::new(&[#iv_token_stream][..]);
            let cipher_bytes = Box::new(&[#cipher_bytes_token_stream][..]);

            let mut result: Result<(), ErrorStack> = Result::Ok(());
            let mut cipher_bytes = BytesMut::from(*cipher_bytes);
            let mut output_buf = Vec::<u8>::new();
            let cipher = Cipher::aes_128_cbc();
            let block_size = cipher.block_size();
            let decrypter = Crypter::new(cipher, Mode::Decrypt, *key, Some(*iv));
            let mut decrypter = match decrypter {
                Ok(unwrapped) => { Option::Some(unwrapped) }
                Err(e) => {
                    result = Err(e);
                    Option::None
                }
            };

            if let Some(ref mut decrypter) = decrypter {
                loop {
                    if !cipher_bytes.has_remaining() { break; }

                    let len = std::cmp::min(512, cipher_bytes.remaining());
                    let slice = Vec::from(&cipher_bytes[0..len]);
                    let mut out_buf = vec![0u8; slice.len() + block_size * 2];
                    let len = decrypter.update(&slice, &mut out_buf);
                    let len = match len {
                        Ok(unwrapped) => { Option::Some(unwrapped) }
                        Err(e) => {
                            result = Err(e);
                            Option::None
                        }
                    };
                    if len.is_none() { break; }
                    let len = len.unwrap();

                    output_buf.extend_from_slice(&out_buf[0..len]);
                    cipher_bytes.advance(slice.len());
                }
            }
            if let Some(ref mut decrypter) = decrypter {
                let mut out_buf = vec![0u8; block_size];
                let len = decrypter.finalize(&mut out_buf);
                let len = match len {
                    Ok(unwrapped) => { Option::Some(unwrapped) }
                    Err(e) => {
                        result = Err(e);
                        Option::None
                    }
                };
                if let Some(len) = len {
                    output_buf.extend_from_slice(&out_buf[0..len]);
                }
            }
            match result {
                Ok(_) => {
                    Ok(Vec::from(&output_buf[..output_buf.len()]))
                }
                Err(e) => {
                    Err(e)
                }
            }
        }}).into();
    }

    fn u8slice_to_token_stream(bytes: &[u8]) -> proc_macro2::TokenStream {
        use proc_macro2::{TokenStream, TokenTree, Literal, Punct, Spacing};

        let mut ts = TokenStream::new();
        for b in bytes {
            let token = TokenTree::Literal(Literal::u8_suffixed(b.to_owned()));
            ts.append(token);
            let token = TokenTree::Punct(Punct::new(',', Spacing::Alone));
            ts.append(token);
        }
        return ts;
    }

    fn rand_bytes(size: usize) -> Vec<u8> {
        let mut bytes = vec![0u8; size];
        openssl::rand::rand_bytes(&mut bytes).unwrap();
        return bytes;
    }

    fn encrypt_plain_bytes(plain_bytes: Vec<u8>) -> (Vec<u8>, Vec<u8>, Vec<u8>) {
        let mut plain_bytes = BytesMut::from(&plain_bytes[..]);
        let key = rand_bytes(16);
        let iv = rand_bytes(16);
        let mut output_buf = Vec::<u8>::new();
        let cipher = Cipher::aes_128_cbc();
        let block_size = cipher.block_size();
        let mut encrypter = Crypter::new(
            cipher, Mode::Encrypt, &key, Some(&iv)).unwrap();

        loop {
            if !plain_bytes.has_remaining() { break; }

            let len = std::cmp::min(512, plain_bytes.len());
            let slice = Vec::from(&plain_bytes[0..len]);
            let mut out_buf = vec![0u8; slice.len() + block_size * 2];
            let len = encrypter.update(&slice, &mut out_buf).unwrap();
            output_buf.extend_from_slice(&out_buf[0..len]);
            plain_bytes.advance(slice.len());
        }
        {
            let mut out_buf = vec![0u8; block_size * 2];
            let len = encrypter.finalize(&mut out_buf).unwrap();
            output_buf.extend_from_slice(&out_buf[0..len]);
        }
        let cipher_bytes = Vec::from(&output_buf[..output_buf.len()]);
        return (key, iv, cipher_bytes);
    }

    #[allow(dead_code)]
    #[allow(unused_imports)]
    fn expected_output_0(key: &[u8], iv: &[u8], cipher_bytes: &[u8]) -> Vec<u8> {
        use openssl::symm::{Crypter, Cipher, Mode};
        use bytes::BytesMut;
        use bytes::buf::Buf;

        // Remove comments after paste into quote!{}.
        // let key = [#key_token_stream].as_ref();
        // let iv = [#iv_token_stream].as_ref();
        // let cipher_bytes = [#cipher_bytes_token_stream].as_ref();

        let mut cipher_bytes = BytesMut::from(cipher_bytes);
        let mut output_buf = Vec::<u8>::new();
        let cipher = Cipher::aes_128_cbc();
        let block_size = cipher.block_size();
        let mut decrypter = Crypter::new(
            cipher, Mode::Decrypt, key, Some(iv)).unwrap();

        loop {
            if !cipher_bytes.has_remaining() { break; }

            let len = std::cmp::min(512, cipher_bytes.remaining());
            let slice = Vec::from(&cipher_bytes[0..len]);
            let mut out_buf = vec![0u8; slice.len() + block_size * 2];
            let len = decrypter.update(&slice, &mut out_buf).unwrap();
            output_buf.extend_from_slice(&out_buf[0..len]);
            cipher_bytes.advance(slice.len());
        }
        {
            let mut out_buf = vec![0u8; block_size];
            let len = decrypter.finalize(&mut out_buf).unwrap();
            output_buf.extend_from_slice(&out_buf[0..len]);
        }
        Vec::from(&output_buf[..output_buf.len()])
    }

    #[allow(dead_code)]
    #[allow(unused_imports)]
    fn expected_output_1(key: Box<&[u8]>, iv: Box<&[u8]>, cipher_bytes: Box<&[u8]>) -> Result<Vec<u8>, ErrorStack> {
        use openssl::symm::{Crypter, Cipher, Mode};
        use bytes::BytesMut;
        use bytes::buf::Buf;
        use openssl::error::ErrorStack;
        use std::result::Result::{Ok, Err};

        /* Remove comments after paste into quote!{}.
           let key = Box::new(&[#key_token_stream][..]);
           let iv = Box::new(&[#iv_token_stream][..]);
           let cipher_bytes = Box::new(&[#cipher_bytes_token_stream][..]);
         */

        let mut result: Result<(), ErrorStack> = Result::Ok(());
        let mut cipher_bytes = BytesMut::from(*cipher_bytes);
        let mut output_buf = Vec::<u8>::new();
        let cipher = Cipher::aes_128_cbc();
        let block_size = cipher.block_size();
        let decrypter = Crypter::new(cipher, Mode::Decrypt, *key, Some(*iv));
        let mut decrypter = match decrypter {
            Ok(unwrapped) => { Option::Some(unwrapped) }
            Err(e) => {
                result = Err(e);
                Option::None
            }
        };

        if let Some(ref mut decrypter) = decrypter {
            loop {
                if !cipher_bytes.has_remaining() { break; }

                let len = std::cmp::min(512, cipher_bytes.remaining());
                let slice = Vec::from(&cipher_bytes[0..len]);
                let mut out_buf = vec![0u8; slice.len() + block_size * 2];
                let len = decrypter.update(&slice, &mut out_buf);
                let len = match len {
                    Ok(unwrapped) => { Option::Some(unwrapped) }
                    Err(e) => {
                        result = Err(e);
                        Option::None
                    }
                };
                if len.is_none() { break; }
                let len = len.unwrap();

                output_buf.extend_from_slice(&out_buf[0..len]);
                cipher_bytes.advance(slice.len());
            }
        }
        if let Some(ref mut decrypter) = decrypter {
            let mut out_buf = vec![0u8; block_size];
            let len = decrypter.finalize(&mut out_buf);
            let len = match len {
                Ok(unwrapped) => { Option::Some(unwrapped) }
                Err(e) => {
                    result = Err(e);
                    Option::None
                }
            };
            if let Some(len) = len {
                output_buf.extend_from_slice(&out_buf[0..len]);
            }
        }
        match result {
            Ok(_) => {
                Ok(Vec::from(&output_buf[..output_buf.len()]))
            }
            Err(e) => {
                Err(e)
            }
        }
    }
}
